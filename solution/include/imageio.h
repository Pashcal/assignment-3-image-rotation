#pragma once
#ifndef _BMP_IO_
#define _BMP_IO_

#include "../include/image.h"
#include <stdio.h>

// reversed BM[P] because of big-endian (first value byte is the last in data)
#define BMP_SIGNATURE 0x4d42

// bmp_header structure
#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;            // 0
    uint32_t bfileSize;         // 2
    uint32_t bfReserved;        // 6
    uint32_t bOffBits;          // 10
    uint32_t biSize;            // 14
    uint32_t biWidth;           // 18
    uint32_t biHeight;          // 22
    uint16_t biPlanes;          // 26
    uint16_t biBitCount;        // 28
    uint32_t biCompression;     // 30
    uint32_t biSizeImage;       // 34
    uint32_t biXPelsPerMeter;   // 38
    uint32_t biYPelsPerMeter;   // 42
    uint32_t biClrUsed;         // 46
    uint32_t biClrImportant;    // 50
};                              // = 54 bytes
#pragma pack(pop)

/*  deserializer   */
enum read_status {
    READ_OK = 0x0,
    READ_INVALID_SIGNATURE = 0x1,
    READ_INVALID_BITS = 0x2,
    READ_INVALID_HEADER = 0x4,
    READ_BODY_ERROR = 0x8,
    READ_PADDING_ERROR = 0x10,
    READ_REALLOCATE_IMAGE_ERROR = 0x20
};

/*  serializer   */
enum write_status {
    WRITE_OK = 0x0,
    WRITE_HEADER_ERROR = 0x1,
    WRITE_BODY_ERROR = 0x2
};

enum read_status from_bmp(FILE* in, struct image* img);
enum write_status to_bmp(FILE* out, struct image const* img);

#endif
