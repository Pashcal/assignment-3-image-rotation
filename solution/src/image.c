#include "../include/image.h"

const struct image invalid_image = { .data = NULL, .width = 0, .height = 0 };

// returns pointer on new image data
struct image new_image(uint32_t width, uint32_t height) {
	struct pixel* img_data = NULL;
	
	if (width > 0 && height > 0) {
		img_data = malloc(sizeof(struct pixel) * width * height);

		// allocate error
		if (img_data == NULL) {
			return invalid_image;
		}
	}

	return (struct image) { .data = img_data, .width = width, .height = height };
}

int is_valid(struct image* img) {
	if (img != NULL && img->width != 0 && img->height != 0 && img->data != NULL) {
		return 1;
	}

	return 0;
}

void remove_image(struct image* img) {
	if (img != NULL && img->data != NULL) {
		free(img->data);
	}
}
