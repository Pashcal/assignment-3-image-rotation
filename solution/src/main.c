#define _CRT_SECURE_NO_WARNINGS
#include "../include/image.h"
#include "../include/imageio.h"
#include "../include/rotate.h"

int main(int argc, char* argv[]) {
    if (argc < 3) {
        fprintf(stderr, "%s\n", "wrong parametres");
        return 1;
    }

    FILE* input = fopen(argv[1], "rb");
    if (input == NULL) {
        fprintf(stderr, "%s: %s\n", "open input file error", argv[1]);
        return 2;
    }

    struct image img = new_image(0, 0);
    enum read_status rs = from_bmp(input, &img);
    if (fclose(input) != 0) {
        fprintf(stderr, "%s: %s\n", "close input file error", argv[2]);
        return 3;
    }

    if (rs != READ_OK) {
        fprintf(stderr, "%s = %d", "read status", rs);
        return 4;
    }

    FILE* output = fopen(argv[2], "wb");
    if (output == NULL) {
        fprintf(stderr, "%s: %s\n", "open output file error", argv[2]);
        return 5;
    }

    struct image rotated_img = rotate(&img);
    remove_image(&img);
    if (!is_valid(&rotated_img)) {
        fprintf(stderr, "%s", "rotated image is invalid");
        return 6;
    }

    enum write_status ws = to_bmp(output, &rotated_img);
    remove_image(&rotated_img);
    if (fclose(output) != 0) {
        fprintf(stderr, "%s: %s\n", "close output file error", argv[1]);
        return 7;
    }

    if (ws != WRITE_OK) {
        fprintf(stderr, "%s = %d", "write status", ws);
        return 8;
    }

    return 0;
}
