#include "../include/imageio.h"

uint8_t get_bmp_padding(uint32_t width) {
    if (width % 4 == 0) {
        return (uint8_t)0;
    }

    return (uint8_t)(4 - ((width * sizeof(struct pixel)) % 4));
}

// for opened file
enum read_status read_bmp_header(FILE* in, struct bmp_header* header) {
    if (in == NULL || header == NULL) {
        return READ_INVALID_HEADER;
    }

    if (fread(header, sizeof(struct bmp_header), 1, in) != 1) {
        return READ_INVALID_HEADER;
    }

    if (header->bfType != BMP_SIGNATURE) {
        return READ_INVALID_SIGNATURE;
    }

    // 24 => 8 bit for r,g,b
    if (header->biBitCount != 24) {
        return READ_INVALID_BITS;
    }

    return READ_OK;

}

// for initialized img
enum read_status read_bmp_body(FILE* in, struct image* img) {
    uint32_t width = img->width;
    uint32_t height = img->height;
    int32_t padding = get_bmp_padding(width);

    uint64_t row_offset = 0;
    uint64_t delta_offset = width;

    for (uint64_t i = 0; i < height; i++) {
        for (uint64_t j = 0; j < width; j++) {
            if (fread(img->data + row_offset + j, sizeof(struct pixel), 1, in) != 1) {
                return READ_BODY_ERROR;
            }
        }

        if (fseek(in, padding, SEEK_CUR) != 0) {
            return READ_PADDING_ERROR;
        }

        row_offset = row_offset + delta_offset;
    }

    return READ_OK;
}

// for opened file
enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header;
    enum read_status results = read_bmp_header(in, &header);

    if (results != READ_OK) {
        return results;
    }

    if (img->width * img->height != header.biWidth * header.biHeight) {
        remove_image(img);
        *img = new_image(header.biWidth, header.biHeight);

        if (!is_valid(img)) {
            return READ_REALLOCATE_IMAGE_ERROR;
        }
    }
    else {
        img->width = header.biWidth;
        img->height = header.biHeight;
    }

    results = read_bmp_body(in, img);

    return results;
}

struct bmp_header new_bmp_header(const struct image* img) {
    return (struct bmp_header) {
        .bfType = BMP_SIGNATURE,
        .bfileSize = sizeof(struct bmp_header) + sizeof(struct pixel) * img->width * img->height + get_bmp_padding(img->width) * img->height,
        .bfReserved = 0,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = sizeof(struct bmp_header) - 14,
        .biWidth = img->width,
        .biHeight = img->height,
        .biPlanes = 1,
        .biBitCount = 24,
        .biCompression = 0,
        .biSizeImage = sizeof(struct pixel) * img->width * img->height + get_bmp_padding(img->width) * img->height,
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0
    };
}

enum write_status write_bmp_header(FILE* out, struct bmp_header* header) {
    if (fwrite(header, sizeof(struct bmp_header), 1, out) != 1) {
        return WRITE_HEADER_ERROR;
    }

    return WRITE_OK;
}

enum write_status write_bmp_body(FILE* out, const struct image* img) {
    uint32_t width = img->width;
    uint32_t height = img->height;
    size_t padding = get_bmp_padding(width);

    uint8_t padding_data = 0;

    uint64_t row_offset = 0;
    uint64_t delta_offset = width;

    for (uint64_t i = 0; i < height; i++) {
        for (uint64_t j = 0; j < width; j++) {
            if (fwrite(img->data + row_offset + j, sizeof(struct pixel), 1, out) != 1) {
                return WRITE_BODY_ERROR;
            }
        }

        for (uint64_t j = 0; j < padding; j++) {
            if (fwrite(&padding_data, sizeof(padding_data), 1, out) != 1) {
                return WRITE_BODY_ERROR;
            }
        }
        row_offset += delta_offset;
    }

    return WRITE_OK;
}

// for opened file
enum write_status to_bmp(FILE* out, struct image const* img) {
    struct bmp_header header = new_bmp_header(img);
    enum write_status results = write_bmp_header(out, &header);

    if (results != WRITE_OK) {
        return results;
    }

    results = write_bmp_body(out, img);

    return results;
}
